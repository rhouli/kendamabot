/* Kendama Bot
 *
 *  File: CTaskOrientationCustom.cpp
 * 
 *  Created on: May 10, 2015
 *
 *  Copyright (C) 2015
 *
 *  Author: Ryan Houlihan <ryan.houlihan@stanford.edu> */

#include "CTaskOrientationCustom.hpp"

#include <scl/Singletons.hpp>
#include <sutil/CRegisteredDynamicTypes.hpp>

#include <stdio.h>
#include <iostream>
#include <stdexcept>
#include <sstream>

#ifdef DEBUG
#include <cassert>
#endif

#include <Eigen/Dense>
//Used only in singularities. Read more in the model update function's comments.
#include <Eigen/SVD>

using namespace scl;

namespace scl_app
{

  CTaskOrientationCustom::CTaskOrientationCustom() :
          CTaskBase(),
          data_(S_NULL),
          use_svd_for_lambda_inv_(false),
          flag_compute_gravity_(false)
  { }

  //************************
  // Inherited stuff
  //************************
  bool CTaskOrientationCustom::init(STaskBase* arg_task_data,
      CDynamicsBase* arg_dynamics)
  {
    try
    {
      if(S_NULL == arg_task_data)
      { throw(std::runtime_error("Passed a null task data structure"));  }

      if(false == arg_task_data->has_been_init_)
      { throw(std::runtime_error("Passed an uninitialized task data structure"));  }

      if(S_NULL == arg_dynamics)
      { throw(std::runtime_error("Passed a null dynamics object"));  }

      if(false == arg_dynamics->hasBeenInit())
      { throw(std::runtime_error("Passed an uninitialized dynamics object"));  }

      data_ = dynamic_cast<STaskOrientationCustom*>(arg_task_data);

      dynamics_ = arg_dynamics;
      data_->rbd_ = data_->gc_model_->rbdyn_tree_.at_const(data_->link_name_);
      if(S_NULL == data_->rbd_)
      { throw(std::runtime_error("Couldn't find link dynamics object")); }

      //Defaults
      singular_values_.setZero();

      //Try to use the householder qr instead of the svd in general
      //Computing this once here initializes memory and resizes qr_
      //It will be used later.
      qr_.compute(data_->M_task_);

      has_been_init_ = true;
    }
    catch(std::exception& e)
    {
      std::cerr<<"\nCTaskOrientationCustom::init() :"<<e.what();
      has_been_init_ = false;
    }
    return has_been_init_;
  }

  STaskBase* CTaskOrientationCustom::getTaskData()
  { return data_; }

  /** Sets the current goal position */
  bool CTaskOrientationCustom::setGoalPos(const Eigen::VectorXd & arg_goal)
  {
    return true;
  }

  /** Sets the current goal velocity */
  bool CTaskOrientationCustom::setGoalVel(const Eigen::VectorXd & arg_goal)
  {
    return true;
  }

  /** Sets the current goal acceleration */
  bool CTaskOrientationCustom::setGoalAcc(const Eigen::VectorXd & arg_goal)
  {
    return true;
  }

  void CTaskOrientationCustom::reset()
  {
    data_ = S_NULL;
    dynamics_ = S_NULL;
    has_been_init_ = false;
  }


  bool CTaskOrientationCustom::computeServo(const SRobotSensors* arg_sensors)
  {
#ifdef DEBUG
    assert(has_been_init_);
    assert(S_NULL!=data_->rbd_ );
    assert(S_NULL!=dynamics_);
#endif
    if(data_->has_been_init_)
    {
      //Step 1: Find pos__
      Eigen::Matrix3d R = data_->rbd_->T_o_lnk_.rotation();
      data_->delta_phi_.setZero(data_->dof_task_);
      for (unsigned int i = 0 ; i < data_->dof_task_; ++i) {
	data_->delta_phi_ += 0.5*(R.col(i).cross(data_->orientation_goal_.col(i)));
      }

      //Global coordinates : dx = J . dq
      data_->dphi_ = data_->J_ * arg_sensors->dq_;

      //Compute the servo torques
      tmp1 = data_->delta_phi_;
      tmp1 = data_->kp_.array() * tmp1.array();

      tmp2 = (data_->dphi_goal_ - data_->dphi_);
      tmp2 = data_->kv_.array() * tmp2.array();

      //Obtain force to be applied to a unit mass floating about
      //in space (ie. A dynamically decoupled mass).
      data_->ddphi_ = data_->ka_.array() * (data_->ddphi_goal_ - data_->ddphi_).array();
      data_->ddphi_ += tmp2 + tmp1;
      
      //Min of self and max
      data_->ddphi_ = data_->ddphi_.array().min(data_->force_task_max_.array());
      //Max of self and min
      data_->ddphi_ = data_->ddphi_.array().max(data_->force_task_min_.array());

      data_->force_task_ = data_->M_task_ * data_->ddphi_;

      // T = J' ( M x F* + p)
      // We do not use the centrifugal/coriolis forces. They can cause instabilities.
      data_->force_gc_ = data_->J_.transpose() * data_->force_task_;
      //      std::cout << "Orientation Force " << data_->force_gc_ << std::endl;
      return true;
    }
    else
    { return false; }
  }

  /** Computes the dynamics (task model)
   * Assumes that the data_->model_.gc_model_ has been updated. */
  bool CTaskOrientationCustom::computeModel(const scl::SRobotSensors* arg_sensors)
  {
#ifdef DEBUG
    assert(has_been_init_);
    assert(data_->has_been_init_);
    assert(S_NULL!=data_->rbd_);
    assert(S_NULL!=dynamics_);
#endif
    if(data_->has_been_init_)
    {
      bool flag = true;
      const SGcModel* gcm = data_->gc_model_;

      flag = flag && dynamics_->computeJacobian(data_->J_,*(data_->rbd_),
          arg_sensors->q_,data_->pos_in_parent_);

      //Use the orientation jacobian only. This is an op-point task.
      data_->J_ = data_->J_.block(3, 0, 3, data_->robot_->dof_);

      //Operational space mass/KE matrix:
      //Lambda = (J * Ainv * J')^-1
      data_->M_task_inv_ = data_->J_ * gcm->M_gc_inv_ * data_->J_.transpose();

      if(!use_svd_for_lambda_inv_)
      {
        //The general inverse function works very well for op-point controllers.
        //3x3 matrix inversion behaves quite well. Even near singularities where
        //singular values go down to ~0.001. If the model is coarse, use a n-k rank
        //approximation with the SVD for a k rank loss in a singularity.
        qr_.compute(data_->M_task_inv_);
        if(qr_.isInvertible())
        { data_->M_task_ = qr_.inverse();  }
        else
        { use_svd_for_lambda_inv_ = true; }
      }

      if(use_svd_for_lambda_inv_)
      {
        //Use a Jacobi svd. No preconditioner is required coz lambda inv is square.
        //NOTE : This is slower and generally performs worse than the simple inversion
        //for small (3x3) matrices that are usually used in op-space controllers.
        svd_.compute(data_->M_task_inv_,
            Eigen::ComputeFullU | Eigen::ComputeFullV | Eigen::ColPivHouseholderQRPreconditioner);

#ifdef DEBUG
        std::cout<<"\n Singular values : "<<svd_.singularValues().transpose();
#endif
        int rank_loss=0;

        //NOTE : A threshold of .005 works quite well for most robots.
        //Experimentally determined: Take the robot to a singularity
        //and observe the response as you allow the min singular values
        //to decrease. Stop when the robot starts to go unstable.
        //NOTE : This also strongly depends on how good your model is
        //and how fast you update it. A bad model will require higher
        //thresholds and will result in coarse motions. A better model
        //will allow much lower thresholds and will result in smooth
        //motions.
        if(svd_.singularValues()(0) > 0.005)
        { singular_values_(0,0) = 1.0/svd_.singularValues()(0);  }
        else { singular_values_(0,0) = 0.0; rank_loss++; }
        if(svd_.singularValues()(1) > 0.005)
        { singular_values_(1,1) = 1.0/svd_.singularValues()(1);  }
        else { singular_values_(1,1) = 0.0; rank_loss++; }
        if(svd_.singularValues()(2) > 0.005)
        { singular_values_(2,2) = 1.0/svd_.singularValues()(2);  }
        else { singular_values_(2,2) = 0.0; rank_loss++; }

        if(0 < rank_loss)
        { std::cout<<"\nCTaskOrientationCustom::computeModel() : Warning. Lambda_inv is ill conditioned. SVD rank loss (@.005) = "<<rank_loss; }

        data_->M_task_ = svd_.matrixV() * singular_values_ * svd_.matrixU().transpose();

        //Turn off the svd after 50 iterations
        //Don't worry, the qr will pop back to svd if it is still singular
        static sInt svd_ctr = 0; svd_ctr++;
        if(50>=svd_ctr)
        { svd_ctr = 0; use_svd_for_lambda_inv_ = false;  }
      }

      //Compute the Jacobian dynamically consistent generalized inverse :
      //J_dyn_inv = Ainv * J' (J * Ainv * J')^-1
      data_->J_dyn_inv_ = gcm->M_gc_inv_ * data_->J_.transpose() * data_->M_task_;

      //J' * J_dyn_inv'
      sUInt dof = data_->robot_->dof_;
      data_->null_space_ = Eigen::MatrixXd::Identity(dof, dof) -
          data_->J_.transpose() * data_->J_dyn_inv_.transpose();

      // We do not use the centrifugal/coriolis forces. They can cause instabilities.
      data_->force_task_cc_.setZero(data_->dof_task_,1);

      // J' * J_dyn_inv' * g(q)
      if(flag_compute_gravity_)
      { data_->force_task_grav_ =  data_->J_dyn_inv_.transpose() * gcm->force_gc_grav_;  }

      return flag;
    }
    else
    { return false; }
  }


  //************************
  // Task specific stuff
  //************************


  /*******************************************
              Dynamic Type : CTaskOrientationCustom

     NOTE : To enable dynamic typing for tasks, you
     must define the types for the "CTaskName" computation
     object AND the "STaskName" data structure. THIS IS NECESSARY.

     Why? So that you have a quick and easy way to specify
     custom xml parameters in the *Cfg.xml file.
   *******************************************/
  scl::sBool registerType_TaskOrientationCustom()
  {
    bool flag;
    try
    {
      sutil::CDynamicType<std::string,scl_app::CTaskOrientationCustom> typeCTaskOrientionCustom(std::string("CTaskOrientationCustom"));
      flag = typeCTaskOrientionCustom.registerType();
      if(false == flag) {throw(std::runtime_error("Could not register type CTaskOrientationCustom"));}

      sutil::CDynamicType<std::string,scl_app::STaskOrientationCustom> typeSTaskOrientationCustom(std::string("STaskOrientationCustom"));
      flag = typeSTaskOrientationCustom.registerType();
      if(false == flag) {throw(std::runtime_error("Could not register type STaskOrientationCustom"));}

#ifdef DEBUG
      std::cout<<"\nregisterType_TaskCustom() : Registered my cool operational space task with the database";
#endif
    }
    catch (std::exception& e)
    {
      std::cout<<"\nregisterType_TaskCustom() : Error : "<<e.what();
      return false;
    }
    return true;
  }
}
