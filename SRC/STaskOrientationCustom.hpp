/* Kendama Bot
 *
 *  File: STaskOrientationCustom.hpp
 * 
 *  Created on: May 10, 2015
 *
 *  Copyright (C) 2015
 *
 *  Author: Ryan Houlihan <ryan.houlihan@stanford.edu> */

#ifndef STASKORIENTATIONCUSTOM_HPP_
#define STASKORIENTATIONCUSTOM_HPP_

#include <scl/DataTypes.hpp>
#include <scl/control/task/data_structs/STaskBase.hpp>

#include <Eigen/Dense>

namespace scl_app
{

  class STaskOrientationCustom : public scl::STaskBase
  {
  public:
    //Computed attributes (last measured, in x dimensional task-space)
    Eigen::VectorXd delta_phi_;             //Position in the global frame
    Eigen::VectorXd dphi_;            //Velocity in the global frame
    Eigen::VectorXd ddphi_;           //Acceleration in the global frame

    Eigen::Matrix3d orientation_goal_;
    Eigen::VectorXd dphi_goal_;       //Goal Velocity in the global frame
    Eigen::VectorXd ddphi_goal_;      //Goal Acceleration in the global frame

    Eigen::Vector3d pos_in_parent_; //Position in the parent link's local frame (x,y,z)

    std::string link_name_;         //The parent link
    const scl::SRigidBody *link_ds_;     //The parent link's parsed data structure

    scl::sFloat spatial_resolution_;     //Meters

    const scl::SRigidBodyDyn *rbd_;   //For quickly obtaining a task Jacobian

    /** Default constructor sets stuff to S_NULL */
    STaskOrientationCustom();


    /** Default destructor does nothing */
    virtual ~STaskOrientationCustom();

    /** 1. Initializes the task specific data members.
     *
     * 2. Parses non standard task parameters,
     * which are stored in STaskBase::task_nonstd_params_.
     * Namely:
     *  (a) parent link name
     *  (b) pos in parent.*/
    virtual bool initTaskParams();
   
    bool setGoalRotationMatrix(Eigen::Matrix3d);
  };

}

#endif /* STASKORIENTATIONCUSTOM_HPP_ */
