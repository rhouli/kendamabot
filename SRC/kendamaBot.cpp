/* Kendama Bot
 *
 *  File: KendamaBot.cpp
 * 
 *  Created on: May 10, 2015
 *
 *  Copyright (C) 2015
 *
 *  Author: Ryan Houlihan <ryan.houlihan@stanford.edu> */


#include "kendamaBot.hpp"

//For timing
#include <sutil/CSystemClock.hpp>

//Standard includes (for printing and multi-threading)
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <omp.h>
#include <unistd.h>
#include <algorithm>
#include <signal.h>
#include <stdio.h>

#include <zmqpp/zmqpp.hpp>

#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/select.h>
#include <termios.h>
#include <stropts.h>
#include <sys/ioctl.h>

#include <boost/program_options.hpp>
#include <time.h>

#define EPSILON 0.00001

bool zmqIntialized = false;

int main(int argc, char** argv){
  std::cout<<"\n***************************************\n";
  std::cout<<"KendamaBot";
  std::cout<<"\n***************************************\n";

  bool flag; 

  scl::SRobotParsed rds;     //Robot data structure....
  scl::SGraphicsParsed rgr;  //Robot graphics data structure...
  scl::SGcModel rgcm;        //Robot data structure with dynamic quantities...
  scl::SRobotIO rio;         //I/O data structure
  scl::CGraphicsChai rchai;  //Chai interface (updates graphics rendering tree etc.)
  scl::CDynamicsScl dyn_scl; //Robot kinematics and dynamics computation object...
  scl::CDynamicsTao dyn_tao; //Robot physics integrator
  scl::CParserScl p;         //This time, we'll parse the tree from a file...

  scl::SControllerMultiTask rctr_ds; //A multi-task controller data structure
  scl::CControllerMultiTask rctr;    //A multi-task controller
  std::vector<scl::STaskBase*> rtasks;              //A set of executable tasks
  std::vector<scl::SNonControlTaskBase*> rtasks_nc; //A set of non-control tasks
  std::vector<scl::sString2> ctrl_params;        //Used to parse extra xml tags
  scl_app::STaskCustom* rtask_position;
  scl_app::STaskOrientationCustom* rtask_orientation; 

  // Reflexxes declerations
  ReflexxesAPI *RML = NULL;
  RMLPositionInputParameters *IP = NULL;
  RMLPositionOutputParameters *OP = NULL;
  RMLPositionFlags RMLFlags;

  double dt = 0.001;

  sutil::CSystemClock::start(); //Start the clock

  std::string filename(argv[1]);

  /******************************Set up Dynamic Type for our new Task************************************/
  flag = scl_app::registerType_TaskCustom(); //Let SCL parse our new task type..
  flag = flag && scl_app::registerType_TaskOrientationCustom(); //Let SCL parse our new task type..x0
  if(false == flag) {  std::cout<<"\n Could not register custom task type\n"; return 1; }

  /******************************Load Robot Specification************************************/
  //We will use a slightly more complex xml spec than the first few tutorials
  const std::string fname("./ROBOT_CFG/KUKA_IIWA/iiwaCfg.xml");
  flag = p.readRobotFromFile(fname,"./ROBOT_CFG","iiwaBot",rds);
  flag = flag && rgcm.init(rds);            //Simple way to set up dynamic tree...
  flag = flag && dyn_tao.init(rds);         //Set up integrator object
  flag = flag && dyn_scl.init(rds);         //Set up kinematics and dynamics object
  flag = flag && rio.init(rds);        //Set up the I/O data structure
  if(false == flag) return 1;           //Error check.

  /******************************Set up Controller Specification************************************/
  // Read xml file info into task specifications.
  flag = p.readTaskControllerFromFile(fname,"opc",rtasks,rtasks_nc,ctrl_params);
  flag = flag && rctr_ds.init("opc",&rds,&rio,&rgcm); //Set up the control data structure..
  // Tasks are initialized after find their type with dynamic typing.
  flag = flag && scl_registry::registerNativeDynamicTypes();
  flag = flag && scl_util::initMultiTaskCtrlDsFromParsedTasks(rtasks,rtasks_nc,rctr_ds);
  flag = flag && rctr.init(&rctr_ds,&dyn_scl);        //Set up the controller (needs parsed data and a dyn object)
  if(false == flag) return 1;            //Error check.

  // set up custom tasks
  rtask_position = dynamic_cast<scl_app::STaskCustom*>( *(rctr_ds.tasks_.at("positionPD")) );
  if(NULL == rtask_position) return 1;       //Error check
  rtask_orientation = dynamic_cast<scl_app::STaskOrientationCustom*>( *(rctr_ds.tasks_.at("orientationPD")) );
  if(NULL == rtask_orientation) return 1;       //Error check

  /******************************ChaiGlut Graphics************************************/
  glutInit(&argc, argv); // We will use glut for the window pane (not the graphics).

  flag = p.readGraphicsFromFile(fname,"iiwaBotStdView",rgr);
  flag = flag && rchai.initGraphics(&rgr);
  flag = flag && rchai.addRobotToRender(&rds,&rio);
  flag = flag && scl_chai_glut_interface::initializeGlutForChai(&rgr, &rchai);
  if(false==flag) { std::cout<<"\nCouldn't initialize chai graphics\n"; return 1; }

  /******************************Read Inputs************************************/
  if(argc != 2) { std::cout << "\nERROR: Wrong number of arguments." << std::endl; return 0; }
  Eigen::VectorXd inital_joint_config; inital_joint_config.setZero(rio.sensors_.q_.size());
  Eigen::VectorXd dump_joint_config; dump_joint_config.setZero(rio.sensors_.q_.size());
  Eigen::VectorXd limits; limits.setZero(9);
  Eigen::VectorXd targets; targets.setZero(6);
  Eigen::Matrix3d R_goal_; R_goal_.setZero(3, 3);
  Eigen::VectorXd ball_info; ball_info.setZero(4);

  bool flag_list[3];
  for(unsigned int i=0;i<rds.dof_;++i){ rio.sensors_.q_(i) = rds.rb_tree_.at(i)->joint_default_pos_; }

  flag = readInputFile(filename, rio, inital_joint_config, limits, targets, ball_info, R_goal_, dt, flag_list);
  if(flag == false) { std::cout << "ERROR: Invalid input file." << std::endl; return 0; }

  dump_joint_config = inital_joint_config;
  dump_joint_config(6) = -inital_joint_config(6);
  //  for(unsigned int i=0;i<rds.dof_;++i){ rio.sensors_.q_(i) = inital_joint_config(i); }

  bool hold_pos = flag_list[0];
  bool straight_traj = flag_list[1];
  bool use_optitrack = flag_list[2];
  // initalize everything
  rctr.computeDynamics();
  rctr.computeControlForces();

  /******************************Kendama Ball************************************/

  chai3d::cGenericObject *kendamaBall_obj;
  double kendamaBall_r = ball_info(3);
  Eigen::Vector3d kendamaBall_pos(ball_info(0)+rtask_position->x_(0), 
				  ball_info(1)+rtask_position->x_(1), 
				  ball_info(2)+rtask_position->x_(2));
  Eigen::Vector3d kendamaBall_vel(0,0,0);

  flag = rchai.addSphereToRender(kendamaBall_pos, kendamaBall_obj, kendamaBall_r);
  if(false == flag){std::cout<<"\nCould not load the kendama ball"; return 1; }

  /******************************Optitrack************************************/
  uint32_t server_ip = inet_addr("172.24.68.48");
  uint32_t local_ip = inet_addr("172.24.68.64");

  // Version number of the NatNet protocol, as reported by the server.
  unsigned char natNetMajor;
  unsigned char natNetMinor;
  
  // Sockets
  int sdCommand;
  int sdData;

  // Use this socket address to send commands to the server.
  struct sockaddr_in serverCommands;
  serverCommands = NatNet::createAddress(server_ip, NatNet::commandPort);
   
  // Create sockets
  sdCommand = NatNet::createCommandSocket(local_ip );
  sdData = NatNet::createDataSocket( local_ip );

  // Start the CommandListener in a new thread.
  CommandListener commandListener(sdCommand);
  commandListener.start();

  // Send a ping packet to the server so that it sends us the NatNet version
  // in its response to commandListener.
  NatNetPacket ping = NatNetPacket::pingPacket();
  ping.send(sdCommand, serverCommands);
   
  // Wait here for ping response to give us the NatNet version.
  commandListener.getNatNetVersion(natNetMajor, natNetMinor);
   
  // Start up a FrameListener in a new thread.
  FrameListener frameListener(sdData, natNetMajor, natNetMinor);
  frameListener.start();

  /******************************Reflexxes************************************/
  RML = new ReflexxesAPI(rtask_position->dof_task_, dt);
  IP = new RMLPositionInputParameters(rtask_position->dof_task_);
  OP = new RMLPositionOutputParameters(rtask_position->dof_task_);

  flag = setLimits(IP, limits, (int)rtask_position->dof_task_);

  // Error checking input values
  if(false == flag){std::cout << "Bad Input Parameters\n"; return 1;}

  /******************************Simulation************************************/
  // Now let us integrate the model for a variety of timesteps and see energy stability
  std::cout<<"\nIntegrating the r6bot's physics. \nWill test two different controllers.\n Press (x) to exit at anytime.";

  /** Gravity applied to the ball **/
  Eigen::Vector3d g(0,0,-9.8);

  while(true){
    std::cout << "\nEnter [d] to dump, [s] to setup, [e] to swing, [t] to toss..." << std::endl;
    char c; c = getchar(); putchar (c);
    std::cout << inital_joint_config.transpose() << std::endl;
    // setup kuka to inital config
    if(c == 's'){
      omp_set_num_threads(2);
      int thread_id; double tstart; flag = false;
      long long iter = 0;
      tstart = sutil::CSystemClock::getSysTime();

#pragma omp parallel private(thread_id)
      {
	thread_id = omp_get_thread_num();
	if(thread_id==1) //Simulate physics and update the rio data structure..
	  {
	    double time_to_config = 2; double iter_to_config = time_to_config/dt;
	    int setup_iter = 0;
	    double kv = 120; double kp = 500;
	    Eigen::VectorXd start_pos = rio.sensors_.q_;
	    Eigen::VectorXd saved_init_config = inital_joint_config;

	    while(true == scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running){ 
	      double frac = (double)setup_iter/iter_to_config;
	      Eigen::VectorXd desired_pos = inital_joint_config*frac + (1-frac)*start_pos;

	      if(setup_iter <= iter_to_config)
		rio.actuators_.force_gc_commanded_ = kp * (desired_pos - rio.sensors_.q_)
		  - kv * rio.sensors_.dq_ - rgcm.force_gc_grav_;
	      else break;

	      dyn_tao.integrate(rio,dt);
	      sendToRobot(rio.sensors_.q_);
	      usleep(dt*1e6);

	      setup_iter++;
	    }
	    //Then terminate
	    scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running = false;
	  } else  //Read the rio data structure and updated rendererd robot..
	  while(true == scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running)
	    { glutMainLoopEvent(); const timespec ts = {0, 15000000};/*15ms*/ nanosleep(&ts,NULL); }
      }
      
      /******************************Exit Gracefully************************************/
      std::cout<<"\n\n\tSystem time = "<<sutil::CSystemClock::getSysTime()-tstart;
      std::cout<<"\n\tSimulated time = "<<static_cast<double>(iter)*dt;
      std::cout<<"\n\nExecuted Successfully";
      std::cout<<"\n**********************************\n"<<std::flush;

    } else if(c == 'd'){

      omp_set_num_threads(2);
      int thread_id; double tstart; flag = false;
      long long iter = 0;
      tstart = sutil::CSystemClock::getSysTime();
      rctr.computeDynamics();
      rctr.computeControlForces();
       
      R_goal_ = rtask_orientation->rbd_->T_o_lnk_.rotation();
      std::cout << "Rotation Goal " << R_goal_ << std::endl;
       
      for(int i = 0; i < 3; ++i)  kendamaBall_pos(i) = ball_info(i) + rtask_position->x_(i);
      kendamaBall_obj->setLocalPos(kendamaBall_pos(0), kendamaBall_pos(1), kendamaBall_pos(2) );	    
      scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running = true;
#pragma omp parallel private(thread_id)
      {
	thread_id = omp_get_thread_num();
	if(thread_id==1) //Simulate physics and update the rio data structure..
	  {
	    double time_to_config = 2; double iter_to_config = time_to_config/dt;
	    int setup_iter = 0;
	    double kv = 120; double kp = 500;
	    Eigen::VectorXd start_pos = rio.sensors_.q_;
	    Eigen::VectorXd saved_init_config = inital_joint_config;

	    while(true == scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running){ 	     
	      double frac = (double)setup_iter/iter_to_config;
	      Eigen::VectorXd desired_pos = dump_joint_config*frac + (1-frac)*start_pos;

	      if(setup_iter <= iter_to_config)
		rio.actuators_.force_gc_commanded_ = kp * (desired_pos - rio.sensors_.q_)
		  - kv * rio.sensors_.dq_ - rgcm.force_gc_grav_;
	      else break;

	      dyn_tao.integrate(rio,dt);
	      sendToRobot(rio.sensors_.q_);
	      usleep(dt*1e6);

	      setup_iter++;
	    }
	    //Then terminate
	    scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running = false;
	  } else  //Read the rio data structure and updated rendererd robot..
	  while(true == scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running)
	    { glutMainLoopEvent(); const timespec ts = {0, 15000000};/*15ms*/ nanosleep(&ts,NULL); }
      }
      /******************************Exit Gracefully************************************/
      std::cout<<"\n\n\tSystem time = "<<sutil::CSystemClock::getSysTime()-tstart;
      std::cout<<"\n\tSimulated time = "<<static_cast<double>(iter)*dt;
      std::cout<<"\n\nExecuted Successfully";
      std::cout<<"\n**********************************\n"<<std::flush;

    } if(c == 'e'){
      omp_set_num_threads(2);
      int thread_id; double tstart; flag = false;
      long long iter = 0;
      // Optitrack
      
      scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running = true;
#pragma omp parallel private(thread_id)
      {
	thread_id = omp_get_thread_num();
	if(thread_id==1) //Simulate physics and update the rio data structure..
	  {
	    rctr.computeDynamics();
	    rctr.computeControlForces();

	    int task_iter = 0;

	    Eigen::VectorXd init_x = rtask_position->x_;

	    rtask_position->x_goal_ = init_x;
	    R_goal_ = rtask_orientation->rbd_->T_o_lnk_.rotation();
	    rtask_orientation->setGoalRotationMatrix(R_goal_);

	    bool valid;
	    MocapFrame frame;
	    std::vector<Point3f> prev_kendama_pos(5, Point3f(1.0, 0.13, 0.0));
	    std::vector<double> kendama_velocity(3, 0.0);
	    std::vector<double> kendama_prev_velocity(3, 0.0);
	    std::vector<Point3f> kendama(2, Point3f(0, 0, 0));
	    Eigen::Vector3d kendamaCup_pos(0.0, 0.0, 0.0);
	    std::vector<double> max_swing(3, 0.0);
	    std::vector<double> min_swing(3, 0.0);

	    kendama[0].x = 1.0; kendama[0].y = 0.13; kendama[0].z = 0.0; 
	    kendama[1].x = 1.0; kendama[1].y = 0.55; kendama[1].z = 0.0; 

	    bool waiting_y, waiting_x, wait_till_line_x, wait_till_line_y;
	    bool is_swinging_forward_y, is_swinging_forward_x, ready_for_pull, ready_for_catch;
	    bool height_achieved, rot_select, computed_landing, stabilize;
	    double waiting, swing_max, swing_dist_x, swing_dist_y, time_to_dist_x, time_to_dist_y;
	    double max_height, tpull, period, swing_dist, time_to_dist, s_period;

	    waiting_y = false; waiting_x = false;
	    wait_till_line_x = false; wait_till_line_y = false;
	    height_achieved = false;
	    ready_for_pull = false; ready_for_catch = false;
	    rot_select = false; computed_landing = false; stabilize = true;

	    waiting = 0; swing_max = 0.015; swing_dist_x = 0.05; swing_dist_y = 0.05;
	    time_to_dist_x = 0.05; time_to_dist_y = 0.05; max_height = 0; tpull = 0;
	    period = 2 * M_PI * sqrt(0.39 / 9.81); swing_dist = 0.03; time_to_dist = 0.0005;
	    s_period = 0;

	    std::cout << "\n *** Beginning stabilizing routine. Hit any key to end: *** \n";
	    while(true == scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running){ 
	      double tcurr = sutil::CSystemClock::getSysTime();

	      if(_kbhit() && stabilize == true){
		stabilize = false;
		waiting_y = false;
		is_swinging_forward_y = false;
		std::cout << "\n *** Stabilizing has completed *** \n";
	      }

	      if(task_iter > 2){
		if(stabilize == true){
		  if(task_iter == 3){
		    if(kendama_prev_velocity[1] < kendama_velocity[1]) 
		      is_swinging_forward_y = true;
		    else is_swinging_forward_y = false;
		  
		    if(kendama_prev_velocity[0] < kendama_velocity[0]) 
		      is_swinging_forward_x = true;
		    else is_swinging_forward_x = false;
		  }
		

		  if(waiting_y) { 
		    if((is_swinging_forward_y == true && 
			(kendama_velocity.at(1) - kendama_prev_velocity.at(1)) < 0) ||
		       (is_swinging_forward_y == false && 
			(kendama_velocity.at(1) - kendama_prev_velocity.at(1)) > 0)){
		    
		      if(is_swinging_forward_y) max_swing[1] = kendamaBall_pos[1];
		      else min_swing[1] = kendamaBall_pos[1];
		      if(max_swing[1] != 0 && min_swing[1] != 0 && is_swinging_forward_y){
			swing_dist_y = fabs(fabs(max_swing[1]) - fabs(min_swing[1]))/2.0;
			if(swing_dist_y > swing_max) swing_dist_y = swing_max;
		      }

		      waiting_y = false;
		      is_swinging_forward_y = !is_swinging_forward_y;
		    }
		  } else if(is_swinging_forward_y){
		    if(kendama[1].x-0.053 > kendama[0].x) wait_till_line_y = true;
		    if(wait_till_line_y){
		      rtask_position->x_goal_(1) -= swing_dist_y*dt/time_to_dist_y;
		      if(rtask_position->x_goal_(1) < init_x(1) - swing_dist_y){
			waiting_y = true;
			wait_till_line_y = false;
		      }
		    }
		  } else if(!is_swinging_forward_y){
		    if(kendama[1].x-0.053 < kendama[0].x) wait_till_line_y = true;
		    if(wait_till_line_y){
		      rtask_position->x_goal_(1) += swing_dist_y*dt/time_to_dist_y;
		      if(rtask_position->x_goal_(1) > init_x(1) + swing_dist_y){
			waiting_y = true;
			wait_till_line_y = false;
		      }
		    }
		  }

		  if(waiting_x) { 
		    if((is_swinging_forward_x == true && 
			(kendama_velocity.at(0) - kendama_prev_velocity.at(0)) > 0) ||
		       (is_swinging_forward_x == false && 
			(kendama_velocity.at(0) - kendama_prev_velocity.at(0)) < 0)){

		      if(is_swinging_forward_x) min_swing[0] = kendamaBall_pos[0];
		      else max_swing[0] = kendamaBall_pos[0];
		      if(max_swing[0] != 0 && min_swing[0] != 0 && is_swinging_forward_x){
			swing_dist_x = fabs(fabs(max_swing[0]) - fabs(min_swing[0]))/2.0;
			if(swing_dist_x > swing_max) swing_dist_x = swing_max;
		      }

		      waiting_x = false;
		      is_swinging_forward_x = !is_swinging_forward_x;
		    }
		  } else if(is_swinging_forward_x){
		    if(kendama[1].z > kendama[0].z) wait_till_line_x = true;
		    if(wait_till_line_x){
		      rtask_position->x_goal_(0) -= swing_dist_x*dt/time_to_dist_x;
		      if(rtask_position->x_goal_(0) < init_x(0) - swing_dist_x){
			waiting_x = true;
			wait_till_line_x = false;
		      }
		    }
		  } else if(!is_swinging_forward_x){
		    if(kendama[1].z < kendama[0].z) wait_till_line_x = true;
		    if(wait_till_line_x){
		      rtask_position->x_goal_(0) += swing_dist_x*dt/time_to_dist_x;
		      if(rtask_position->x_goal_(0) > init_x(0) + swing_dist_x){
			waiting_x = true;
			wait_till_line_x = false;
		      }
		    }
		  }
		} else {
		  if(s_period < 50){
		    if(waiting_y) { 
		      if((is_swinging_forward_y == true && (kendama_velocity.at(1) < 0)) ||
			 (is_swinging_forward_y == false && (kendama_velocity.at(1) > 0))){
			waiting_y = false;
			is_swinging_forward_y = !is_swinging_forward_y;
			if(height_achieved == true && !is_swinging_forward_y){
			  ready_for_pull = true; 
			} else if(height_achieved == true && is_swinging_forward_y){
			  break;
			}
			
			if(is_swinging_forward_y)
			  std::cout << "Swing forward: x:(" << kendama_velocity.at(0) 
				    << ") y:(" << kendama_velocity.at(1) 
				    << ") z:(" << kendama_velocity.at(2) << ")\n";
			else
			  std::cout << "Swing backward: x:(" << kendama_velocity.at(0) 
				    << ") y:(" << kendama_velocity.at(1) 
				    << ") z:(" << kendama_velocity.at(2) << ")\n";
		      }
		    } else if(ready_for_catch){
		      double time_to_pos = 0.3;
		      double dist_x = 0.025;
		      double dist_y = 0.026;
		      double dist_z = 0.225;
			
		      Eigen::Matrix3d R_goal_right;
		      R_goal_right << -0.999293, -0.0305121, -0.0219804,
			-0.020977, -0.0328248, 0.999241,
			-0.0312105, 0.998995, 0.0321615;

		      Eigen::Matrix3d R_goal_left;
		      R_goal_left << -0.995994, 0.00780261, 0.0890835,
			-0.0891838, -0.0136109, -0.995922,
			-0.00655829, -0.999877, 0.0142522;

		      if(rtask_position->x_goal_(0) < init_x(0) + dist_x)
			rtask_position->x_goal_(0) += dist_x*dt/time_to_pos; 
		      
		      if(rtask_position->x_goal_(1) > init_x(1) - dist_y)
			rtask_position->x_goal_(1) -= dist_y*dt/time_to_pos;

		      if(rtask_position->x_goal_(2) > init_x(2) - dist_z)
			rtask_position->x_goal_(2) -= dist_z*dt/time_to_pos; 
			
		      if(!computed_landing){
			init_x = rtask_position->x_;

			computed_landing = true;

			double a = 0.5 * -9.81;
			double b = kendama_velocity.at(2);
			double c = - kendamaCup_pos(2) + kendamaBall_pos(2);

			std::cout << "kendama vel " << kendama_velocity.at(0) << " "
				  << kendama_velocity.at(1) << " "
				  << kendama_velocity.at(2)
				  << std::endl;
			std::cout << "Kendama ball pos " << kendamaBall_pos << std::endl;
			std::cout << "kendama stick pos " << kendamaCup_pos << std::endl;

			double t_to_height = (-b - sqrt(pow(b,2) - 4*a*c))/(2*a);
			
			std::cout << "pow b  " << pow(b,2)-4*a*c << std::endl;
			std::cout << "Time to height " << t_to_height << std::endl;
		      
			for(int i = 0; i < 3; ++i)
			  kendamaBall_vel[i] = kendama_velocity.at(i);
			  
			Eigen::Vector3d landing_pos = kendamaBall_pos 
			  + kendamaBall_vel*t_to_height
			  + 0.5*g*pow(t_to_height, 2);
			
			std::cout << "Landing Position " << landing_pos << std::endl;
		      }
		    } else if(ready_for_pull){
		      if(((is_swinging_forward_y == true && 
			   (kendama_velocity.at(1)) > 0) ||
			  (is_swinging_forward_y == false && 
			   (kendama_velocity.at(1) < 0))) //- kendama_prev_velocity.at(0)) > 0))
			  && areSame(tpull, 0)) {
			tpull = sutil::CSystemClock::getSysTime();
			init_x = rtask_position->x_;
		      }

		      if(tpull > 0){
			double theta;
			double time_to_pull = 0.675;
			double factor = 2.3;
			std::cout << "tcurr-tpull / time_to_dist : " << (tcurr-tpull)/time_to_pull 
				  << std::endl; 
			if((tcurr-tpull)/time_to_pull > 1){
			  ready_for_pull = false;
			  ready_for_catch = true;
			} else if(is_swinging_forward_y){
			  theta = 3.325 * (1 - (tcurr-tpull)/time_to_pull) 
			    - 1.13 * ( (tcurr-tpull)/time_to_pull );

			  rtask_position->x_goal_(1) = init_x(1) -
			    factor*(0.06 * cos(theta) + 0.03 * sin(theta));
			  rtask_position->x_goal_(2) = init_x(2) +
			    0.03 * cos(theta) + 0.06 * sin(theta) + 0.04;

			  if(rot_select == false && (tcurr-tpull)/time_to_pull > 0.65){
			    rot_select = true;
			    Eigen::Matrix3d R_goal_right;

			    R_goal_right <<  0.550844, -0.0664138, 0.831961,
			      -0.831347, 0.044373, 0.55398,
			      -0.0737085, -0.996805, -0.0307703;

			    rtask_orientation->setGoalRotationMatrix(R_goal_right);
			  }
			} else {			  
			  theta = 3.325 * (1 - (tcurr-tpull)/time_to_pull) 
			    - 1.13 * ( (tcurr-tpull)/time_to_pull );
			  
			  rtask_position->x_goal_(1) = init_x(1) +
			    factor*(0.06 * cos(theta) + 0.03 * sin(theta)) + 0.06;
			  rtask_position->x_goal_(2) = init_x(2) +
			    0.03 * cos(theta) + 0.06 * sin(theta) + 0.04; 

			  if(rot_select == false && (tcurr-tpull)/time_to_pull > 0.6){
			    rot_select = true;
			    Eigen::Matrix3d R_goal_left;
			    // 55 degrees
			    R_goal_left << -0.66347, -0.0586906, 0.745897,
			      -0.748198, 0.0483644, -0.661711,
			      0.00276137, -0.997104, -0.0760005;

			    rtask_orientation->setGoalRotationMatrix(R_goal_left);
			  }
			}
		      }
		    } else if(s_period < 1){
		      if(waiting > 0){
			std::cout << "Waiting" << std::endl;
			waiting -= dt;
		      } else if(is_swinging_forward_y){
			std::cout << "Swinging forward" << std::endl;
			rtask_position->x_goal_(1) += swing_dist*dt/time_to_dist;
			if(rtask_position->x_goal_(1) > init_x(1) + swing_dist){
			  s_period += 1;
			  if(s_period == 2){ waiting_y = true; }
			  else {
			    waiting = period/2;
			    is_swinging_forward_y = !is_swinging_forward_y;
			  }
			}
		      } else if(!is_swinging_forward_y){
			std::cout << "Swinging backward" << std::endl;
			rtask_position->x_goal_(1) -= swing_dist*dt/time_to_dist;
			if(rtask_position->x_goal_(1) < init_x(1) - swing_dist){
			  waiting = period/2;
			  is_swinging_forward_y = !is_swinging_forward_y;
			}
		      } 
		    }
		    else if(is_swinging_forward_y){
		      rtask_position->x_goal_(1) += swing_dist*dt/time_to_dist;
		      if(rtask_position->x_goal_(1) > init_x(1) + swing_dist){
			s_period += 1;
			waiting_y = true;
		      }
		    } else if(!is_swinging_forward_y){
		      rtask_position->x_goal_(1) -= swing_dist*dt/time_to_dist;
		      if(rtask_position->x_goal_(1) < init_x(1) - swing_dist){
			waiting_y = true;
		      }
		    }
		  }
		}
	      }
		  
	      rctr.computeDynamics();
	      rctr.computeControlForces();	      

	      /* Integrate the dynamics */
	      dyn_tao.integrate(rio,dt); task_iter++;
	      if(task_iter%2 == 0)
		sendToRobot(rio.sensors_.q_);

	      // Try to get a new frame from the listener.
	      MocapFrame frame(frameListener.pop(&valid).first);
	      // Quit if the listener has no more frames.
	      if( valid ){
		kendama_prev_velocity = kendama_velocity;
		std::vector<Point3f> UIDMarkers = frame.getUIDMarker();
		std::vector<int> index(2, 0);
		std::vector<double> min_element(2, 100000000);
		// find kendama B
		for(unsigned int j = 0; j < kendama.size(); ++j){
		  for(unsigned int i = 0; i < UIDMarkers.size(); ++i){
		    double elem = sqrt(pow(kendama[j].x-UIDMarkers[i].x, 2) 
				       + pow(kendama[j].y-UIDMarkers[i].y, 2) 
				       + pow(kendama[j].z-UIDMarkers[i].z, 2));
		    
		    if(elem < min_element[j]){
		      min_element[j] = elem;
		      index[j] = i;
		    }
		  }
		}
		
		if(index[0] != index[1]){
		  kendama[0] = UIDMarkers[index[0]];
		  update_kendama_vel(&kendama_velocity, &prev_kendama_pos, kendama[0], dt);
		  kendama[1] = UIDMarkers[index[1]];
		} else {
		  if(min_element[0] < min_element[1]){
		    kendama[0] = UIDMarkers[index[0]];
		    update_kendama_vel(&kendama_velocity, &prev_kendama_pos, kendama[0], dt);
		  }
		  else kendama[1] = UIDMarkers[index[1]];
		}
	      }
	      
	      /* Show the kendama ball */
	      kendamaBall_pos[0] = kendama[0].z + 0.64;
	      kendamaBall_pos[1] = (kendama[0].x - 0.98);
	      kendamaBall_pos[2] = kendama[0].y;

	      kendamaCup_pos[0] = kendama[1].z + 0.64 - 0.05;
	      kendamaCup_pos[1] = (kendama[1].x - 0.98 + 0.032);
	      kendamaCup_pos[2] = kendama[1].y;

	      if(kendamaBall_pos[2] >= kendama[1].y - 0.1) height_achieved = true;
	      if(kendamaBall_pos[2] > max_height) max_height = kendamaBall_pos[2];

	      kendamaBall_obj->setLocalPos(kendamaBall_pos[0],kendamaBall_pos[1],kendamaBall_pos[2]);
	      	      
	      usleep(dt*1e6);
	    }
	    //Then terminate
	    scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running = false;
	  } else  //Read the rio data structure and updated rendererd robot..
	  while(true == scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running)
	    { glutMainLoopEvent(); const timespec ts = {0, 15000000};/*15ms*/ nanosleep(&ts,NULL); }
      }
      
      /******************************Exit Gracefully************************************/
      std::cout<<"\n\n\tSystem time = "<<sutil::CSystemClock::getSysTime()-tstart;
      std::cout<<"\n\tSimulated time = "<<static_cast<double>(iter)*dt;
      std::cout<<"\n\nExecuted Successfully";
      std::cout<<"\n**********************************\n"<<std::flush;
    }  else if(c == 't'){

      omp_set_num_threads(2);
      int thread_id; double tstart; flag = false;
      long long iter = 0;
  
       rctr.computeDynamics();
       rctr.computeControlForces();
       
       R_goal_ = rtask_orientation->rbd_->T_o_lnk_.rotation();
       std::cout << "Rotation Goal " << R_goal_ << std::endl;
       
       for(int i = 0; i < 3; ++i)  kendamaBall_pos(i) = ball_info(i) + rtask_position->x_(i);
       kendamaBall_obj->setLocalPos(kendamaBall_pos(0), kendamaBall_pos(1), kendamaBall_pos(2) );    
       scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running = true;
#pragma omp parallel private(thread_id)
       {
	 thread_id = omp_get_thread_num();
	 if(thread_id==1) //Simulate physics and update the rio data structure..
	   {
	     tstart = sutil::CSystemClock::getSysTime(); iter = 0;

	     // Controller : Operational space controller
	     std::cout<<"\n\n***************************************************************"
		      <<"\n Starting op space (task coordinate) controller..."
		      <<"\n***************************************************************";

	     flag = launchBallTask(IP, OP, RML, RMLFlags, rctr, dyn_tao, rio, 
				   rtask_position, rtask_orientation, kendamaBall_obj, 
				   kendamaBall_pos, kendamaBall_vel, targets, R_goal_, 
				   g, kendamaBall_r, &iter, dt, 
				   straight_traj, hold_pos, use_optitrack);

	     if(flag == false){std::cout << "Error with launch ball task " << flag <<  std::endl;}

	     flag = catchBallTask(IP, OP, RML, RMLFlags, rctr, dyn_tao, rio, 
				  rtask_position, rtask_orientation,
				  kendamaBall_obj, kendamaBall_pos, kendamaBall_vel, ball_info, 
				  targets, R_goal_, g, kendamaBall_r, 
				  &iter, dt, straight_traj, hold_pos, use_optitrack);

	     if(flag == false){std::cout << "Error with catch ball task" << std::endl;} 
    
	     scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running = false;
	   } else  //Read the rio data structure and updated rendererd robot..
	   while(true == scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running)
	     { glutMainLoopEvent(); const timespec ts = {0, 15000000};/*15ms*/ nanosleep(&ts,NULL); }
       } 
       /******************************Exit Gracefully************************************/
       std::cout<<"\n\n\tSystem time = "<<sutil::CSystemClock::getSysTime()-tstart;
       std::cout<<"\n\tSimulated time = "<<static_cast<double>(iter)*dt;
       std::cout<<"\n\nExecuted Successfully";
       std::cout<<"\n**********************************\n"<<std::flush;

    }
  }

  // Wait for threads to finish.
  frameListener.stop();
  commandListener.stop();
  frameListener.join();
  commandListener.join();
   
  // Epilogue
  close(sdData);
  close(sdCommand);

  delete RML;
  delete IP;
  delete OP;

  return 0;
}

int _kbhit() {
  static const int STDIN = 0;
  static bool initialized = false;

  if (! initialized) {
    // Use termios to turn off line buffering
    termios term;
    tcgetattr(STDIN, &term);
    term.c_lflag &= ~ICANON;
    tcsetattr(STDIN, TCSANOW, &term);
    setbuf(stdin, NULL);
    initialized = true;
  }

  int bytesWaiting;
  ioctl(STDIN, FIONREAD, &bytesWaiting);
  return bytesWaiting;
}

void update_kendama_vel(std::vector<double> *kendama_velocity, 
			std::vector<Point3f> *prev_kendama_pos, 
			const Point3f& current_pos,
			const double& dt){
  prev_kendama_pos->pop_back();
  prev_kendama_pos->insert(prev_kendama_pos->begin(), current_pos);
  
  Point3f distance(0.0, 0.0, 0.0);
  double time = dt*prev_kendama_pos->size();

  distance.x = prev_kendama_pos->at(0).x - prev_kendama_pos->at(4).x;
  distance.y = prev_kendama_pos->at(0).y - prev_kendama_pos->at(4).y;
  distance.z = prev_kendama_pos->at(0).z - prev_kendama_pos->at(4).z;

  kendama_velocity->at(0) = (distance.z/time);
  kendama_velocity->at(1) = (distance.x/time);
  kendama_velocity->at(2) = (distance.y/time);
}

bool launchBallTask(RMLPositionInputParameters* IP,
		    RMLPositionOutputParameters* OP,
		    ReflexxesAPI* RML,
		    RMLPositionFlags& RMLFlags,
		    scl::CControllerMultiTask& rctr, 
		    scl::CDynamicsTao& dyn_tao,
		    scl::SRobotIO& rio,
		    scl_app::STaskCustom* rtask_position,
		    scl_app::STaskOrientationCustom* rtask_orientation,
		    chai3d::cGenericObject* kendamaBall_obj,
		    Eigen::MatrixBase<Eigen::Vector3d>& kendamaBall_pos,
		    Eigen::MatrixBase<Eigen::Vector3d>& kendamaBall_vel, 
		    Eigen::VectorXd& targets,
		    Eigen::Matrix3d& R_goal_, 
		    const Eigen::Vector3d& g, 
		    const double& kendamaBall_r,
		    long long int* iter,
		    const double& dt,
		    const bool& straight_traj,
		    const bool& hold_pos,
		    const bool& use_optitrack)
{
  Eigen::Vector3d prev_vel; prev_vel.setZero(3);
  Eigen::Vector3d delta_x; delta_x.setZero(3);
  Eigen::Vector3d goal_pos; goal_pos.setZero(3);
  Eigen::Vector3d iter_till_goal; iter_till_goal.setZero(3);
  Eigen::Vector3d final_pos; final_pos.setZero(3);

  int task_iter = 0;
  bool done_lift = false;
  bool flag = false;
  bool done = false;
  delta_x = targets.segment(0,3);
  goal_pos = rtask_position->x_;
  final_pos = rtask_position->x_ + delta_x;

  /* Find our trajectory */
  if(straight_traj == true){
    flag = findStraightTrajectory(iter_till_goal, targets.segment(0,3), 
				  targets.segment(3,3), dt); 
  } else if(hold_pos == false){
    for(unsigned int i = 0; i < rtask_position->dof_task_; ++i){
      IP->CurrentPositionVector->VecData[i] = rtask_position->x_(i);
      IP->CurrentVelocityVector->VecData[i] = 0;
      IP->CurrentAccelerationVector->VecData[i] = 0;
    }    
    flag = findReflexxesTrajectory(*rtask_position, targets, IP);
  } else flag = true;
  if(flag == false) { printf("An error occurred creating trajectory.\n"); return 1;}

  /* Set goal orientation */
  rtask_orientation->setGoalRotationMatrix(R_goal_);

  while(true == scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running){ 
    //    double tcurr = sutil::CSystemClock::getSysTime();

    /* Update trajectory */
    if(straight_traj == true){
      flag = updateStraightTrajectory(iter_till_goal, goal_pos, delta_x, task_iter, 0);
      //      if(flag == false){ return true; }
    } else if(hold_pos == false){

      if(done == false) flag = updateReflexxesTrajectory(IP, OP, RML, RMLFlags);
      if(flag == ReflexxesAPI::RML_FINAL_STATE_REACHED){
	done = true;
      }
    }

    /* Move the hand to next position along trajectory */
    for(unsigned int i = 0; i < rtask_position->dof_task_; ++i){
      if(hold_pos == true) rtask_position->x_goal_(i) =  rtask_position->x_(i);
      else if(straight_traj == true) rtask_position->x_goal_(i) = goal_pos(i);
      else rtask_position->x_goal_(i) = OP->NewPositionVector->VecData[i];
    }
    std::cout << "\n X Goal " << rtask_position->x_goal_.transpose() << std::endl;
    /* Compute control forces (note that these directly have access to the io data ds). */
    rctr.computeDynamics();
    rctr.computeControlForces();

    /* Integrate the dynamics */
    dyn_tao.integrate(rio,dt); *iter++; task_iter++;
    if(task_iter%4 == 0) sendToRobot(rio.sensors_.q_);
    /* Manually integrate kendama ball's position or use optitrack */
    if(hold_pos == false && use_optitrack == false){
      if(rtask_position->dx_(2) >= prev_vel(2) && done_lift == false){
	kendamaBall_vel = rtask_position->dx_;
	kendamaBall_pos += dt*rtask_position->dx_;
	prev_vel = rtask_position->dx_;
      } else {
	if(done_lift != true) done_lift = true;
	kendamaBall_vel += dt*g;
	kendamaBall_pos += dt*kendamaBall_vel;
      }
      prev_vel = rtask_position->dx_;
    } else if(use_optitrack == true){

    }
    // Update graphics
    kendamaBall_obj->setLocalPos(kendamaBall_pos[0],kendamaBall_pos[1],kendamaBall_pos[2]);

    //    flag = slowDownSim(dt, tcurr);
    //    if (flag == false) { printf("An error occurred slowing down simulation.\n"); return 1; }
    usleep(dt*1e6);
  }
  return true;
}

bool catchBallTask(RMLPositionInputParameters* IP,
		   RMLPositionOutputParameters* OP,
		   ReflexxesAPI* RML,
		   RMLPositionFlags& RMLFlags,
		   scl::CControllerMultiTask& rctr,
		   scl::CDynamicsTao& dyn_tao,
		   scl::SRobotIO& rio,
		   scl_app::STaskCustom* rtask_position,
		   scl_app::STaskOrientationCustom* rtask_orientation,
		   chai3d::cGenericObject* kendamaBall_obj,
		   Eigen::MatrixBase<Eigen::Vector3d>& kendamaBall_pos,
		   Eigen::MatrixBase<Eigen::Vector3d>& kendamaBall_vel,
		   const Eigen::VectorXd& ball_info,
		   Eigen::VectorXd& targets,
		   Eigen::Matrix3d& R_goal_,
		   const Eigen::Vector3d& g,
		   const double& kendamaBall_r,
		   long long int* iter,
		   const double& dt,
		   const bool& straight_traj,
		   const bool& hold_pos,
		   const bool& use_optitrack)
{
  Eigen::Vector3d delta_x; delta_x.setZero(3);
  Eigen::Vector3d goal_pos; goal_pos.setZero(3);
  Eigen::Vector3d iter_till_goal; iter_till_goal.setZero(3);
  goal_pos = rtask_position->x_;

  int task_iter = 0;
  int iter_between_update = 0;
  int update_freq = 100;
  bool flag = false;

  /* Set goal orientation */
  rtask_orientation->setGoalRotationMatrix(R_goal_);

  while(true == scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running){ 
    //    double tcurr = sutil::CSystemClock::getSysTime();

    if(task_iter%update_freq == 0){
      if(task_iter > 0) iter_between_update += update_freq;
      flag = computeKendamaBallTrajectory(delta_x, iter_till_goal, kendamaBall_pos, 
					  kendamaBall_vel, rtask_position->x_, ball_info.segment(0, 3), g);

      //      for(int i = 0; i < 3; ++i) delta_x(i) += ball_info(i);
      if(flag == false) { printf("An error occurred computing kendama trajectory.\n"); return 1; }

      /* Find our trajectory */
      if(straight_traj == true){
	//	std::cout << "Find traj " << task_iter << std::endl;
	goal_pos = rtask_position->x_;
	flag = findStraightTrajectory(iter_till_goal, delta_x, targets.segment(3,3), dt); 
      } else if(hold_pos == false){
	flag = findReflexxesTrajectory(*rtask_position, targets, IP);
      } else flag = true;
      if(flag == false) { printf("An error occurred creating trajectory.\n"); return 1; }
    }

    /* Update trajectory */
    if(straight_traj == true){
      flag = updateStraightTrajectory(iter_till_goal, goal_pos, delta_x, 
				      task_iter, iter_between_update);
      //      if(flag == false) return 1;
    } else if(hold_pos == false){
      flag = updateReflexxesTrajectory(IP, OP, RML, RMLFlags);
      if(flag == false) return 1;
    }

    /* Move the hand to next position along trajectory */
    for(unsigned int i = 0; i < rtask_position->dof_task_; ++i){
      if(hold_pos == true) rtask_position->x_goal_(i) =  rtask_position->x_(i);
      else if(straight_traj == true) rtask_position->x_goal_(i) = goal_pos(i);
      else rtask_position->x_goal_(i) = OP->NewPositionVector->VecData[i];
    }

    /* Compute control forces (note that these directly have access to the io data ds). */
    rctr.computeDynamics();
    rctr.computeControlForces();

    /* Integrate the dynamics */
    dyn_tao.integrate(rio,dt); *iter++; task_iter++;

    /* Update kendama ball. Catch it if it is near the cup */
    if(use_optitrack == false){

    }

    // Update graphics
    kendamaBall_obj->setLocalPos(kendamaBall_pos[0],kendamaBall_pos[1],kendamaBall_pos[2]);

    usleep(dt*1e6);

    //    flag = slowDownSim(dt, tcurr);
    //    if (flag == false) { printf("An error occurred slowing down simulation.\n"); return 1; }
  }

  return true;
}

bool computeKendamaBallTrajectory(Eigen::MatrixBase<Eigen::Vector3d>& delta_x,
				  Eigen::MatrixBase<Eigen::Vector3d>& iter_till_goal,
				  const Eigen::Vector3d& kendamaBall_pos,
				  const Eigen::Vector3d& kendamaBall_vel,
				  const Eigen::Vector3d& rtask_position,
				  const Eigen::Vector3d& kendama_pos,
				  const Eigen::Vector3d& g)
{
  double a = g(2);
  double b = kendamaBall_vel(2);
  double c = kendamaBall_pos(2) - (rtask_position(2) + kendama_pos(2));
  double t_to_height_goal = 0;
  if(areSame(kendamaBall_vel(2), 0)) t_to_height_goal = 0;
  else t_to_height_goal = (-b - sqrt(pow(b,2)-4*a*c))/(2*a);

  std::cout << " height ball " << kendama_pos.transpose() << " position " << (rtask_position + kendama_pos).transpose() << std::endl;
  std::cout << "\nTime to Height " << t_to_height_goal << std::endl;
  std::cout << "Vel " << kendamaBall_vel.transpose() << " pos " <<  std::endl;

  /* Track the kendama ball */
  for(int i = 0; i < 2; ++i) 
    delta_x(i) = (kendamaBall_pos(i) + kendamaBall_vel(i)*t_to_height_goal) - (rtask_position(i) + kendama_pos(i));
  delta_x(2) = 0;
  std::cout << "delta x " << delta_x.transpose() << std::endl;
  return true;
}

/******************************Straight Traj***********************************/
bool findStraightTrajectory(Eigen::MatrixBase<Eigen::Vector3d>& iter_till_goal, 
			    const Eigen::Vector3d& delta_pos,
			    const Eigen::Vector3d& vel,
			    const double& dt) {
  for(int i = 0; i < 3; ++i){
    if(!areSame(vel(i),0)) iter_till_goal(i) = abs(delta_pos(i)/(vel(i)*dt));
    else { std::cout << "Velocity can not be zero for straight trajectory" << std::endl; return false; }
  }
  std::cout << "\n Creating a new straight line Trajectory" << std::endl;
  std::cout << "Goal position: change " << delta_pos.transpose() << std::endl;
  std::cout << "Goal velocity: " << vel.transpose() << std::endl;
  std::cout << "Number of iterations " << iter_till_goal.transpose() << std::endl;

  return true;
}

bool updateStraightTrajectory(Eigen::MatrixBase<Eigen::Vector3d>& iter_till_goal, 
			      Eigen::MatrixBase<Eigen::Vector3d>& goal_pos,
			      Eigen::MatrixBase<Eigen::Vector3d>& delta_x,
			      const long long int& task_iter,
			      const long long int& iter_since_update)
{
  bool not_done = false;
  for(int i = 0; i < goal_pos.size(); ++i) {
    // increment goal position by x^2*dt/v
    if(iter_till_goal(i) != 0 && iter_till_goal(i) + iter_since_update > task_iter ){
      goal_pos(i) += delta_x(i)/iter_till_goal(i);
      not_done = true;
    }  
  }

  return not_done;
}

bool findReflexxesTrajectory(const scl_app::STaskCustom& rtask_position,
			     const Eigen::VectorXd& targets,
			     RMLPositionInputParameters* IP)
{
  Eigen::VectorXd targets_new; targets_new = targets;  
  for (int i = 0; i < targets_new.size(); ++i) targets_new(i) += rtask_position.x_(i);
  bool flag = setPosVelTargets(IP, targets_new, (int)rtask_position.dof_task_);

  // Error checking input values
  if(false == flag){std::cout << "Bad Input Parameters\n"; return 1;}
  if(IP->CheckForValidity()) printf("Input values are valid!\n");
  else { printf("Input values are INVALID!\n"); return 1;}

  return true;
}

int updateReflexxesTrajectory(RMLPositionInputParameters* IP,
			      RMLPositionOutputParameters* OP,
			      ReflexxesAPI* RML, 
			      RMLPositionFlags& RMLFlags)
{
  /*  Calling the Reflexxes OTG algorithm to calculate next iteration of trajectory */
  bool flag; 
  flag = RML->RMLPosition(*IP, OP, RMLFlags);                                            
  if (flag < 0) { printf("An error occurred (%d).\n", flag ); return false; }

  *IP->CurrentPositionVector = *OP->NewPositionVector;
  *IP->CurrentVelocityVector = *OP->NewVelocityVector;
  *IP->CurrentAccelerationVector = *OP->NewAccelerationVector;

  return flag;
}

/** Slow down sim to real time */
bool slowDownSim(double dt, double tcurr){
  sutil::CSystemClock::tick(dt);
  double tdiff = sutil::CSystemClock::getSimTime() - tcurr;
  timespec ts = {0, 0};
  if(tdiff > 0){
    ts.tv_sec = static_cast<int>(tdiff);
    tdiff -= static_cast<int>(tdiff);
    ts.tv_nsec = tdiff*1e9;
    nanosleep(&ts,NULL);
  }
  return true;
}

bool setLimits(RMLPositionInputParameters* IP, Eigen::VectorXd limits, int dof){
  if(limits.size() != 3*dof) return false;

  for(int i = 0; i < limits.size()/3; ++i){
    IP->MaxVelocityVector->VecData[i] = limits(i);
    IP->MaxAccelerationVector->VecData[i] = limits(i+dof);
    IP->MaxJerkVector->VecData[i] = limits(i+2*dof);
  }

  IP->SelectionVector->VecData[0] = true;
  IP->SelectionVector->VecData[1] = true;
  IP->SelectionVector->VecData[2] = true;
  return true;
}

bool setPosVelTargets(RMLPositionInputParameters* IP, Eigen::VectorXd targets, int dof){
  if(targets.size() != 2*dof) return false;

  for(int i = 0; i < targets.size()/2; ++i){
    IP->TargetPositionVector->VecData[i] = targets(i);
    IP->TargetVelocityVector->VecData[i] = targets(i+dof);
  }
  IP->TargetVelocityVector->VecData[0] = 0;
  IP->TargetVelocityVector->VecData[1] = 0;

  return true;
}

bool isFloat( std::string myString ) {
  std::istringstream iss(myString);
  float f;
  iss >> std::noskipws >> f; 
  return iss.eof() && !iss.fail(); 
}

bool areSame(double a, double b)
{
  return fabs(a - b) < EPSILON;
}

bool readInputFile(std::string filename, scl::SRobotIO& rio, Eigen::MatrixBase<Eigen::VectorXd>& inital_joint_config, 
		   Eigen::MatrixBase<Eigen::VectorXd>& limits, Eigen::MatrixBase<Eigen::VectorXd>& targets, 
		   Eigen::MatrixBase<Eigen::VectorXd>& ball_info, Eigen::MatrixBase<Eigen::Matrix3d>& R_goal_, 
		   double& dt, bool *flag_list){

  std::ifstream file; std::string word;
  file.open(filename);
  if(!file.is_open()) { std::cout << "ERROR: Invalid input file" << std::endl; return 0; }

  while(file >> word){
    if(word == "inital_joint_config"){
      for(int i = 0; i < rio.sensors_.q_.size() ; ++i){
	file >> word;
	if(!isFloat(word) || file == NULL) 
	  { std::cout << "ERROR: Invalid inital joint config file" << std::endl; return 0; }
	inital_joint_config(i) = (M_PI*std::stof(word.c_str()))/180;	
      }
    } else if(word == "dt"){
      file >> word;
      if(!isFloat(word) || file == NULL) 
	{ std::cout << "ERROR: Invalid dt file" << std::endl; return 0; }
      dt = std::stof(word.c_str());
    } else if(word == "max_vel"){
      for(int i = 0; i < 3; ++i){
	file >> word;
	if(!isFloat(word) || file == NULL) 
	  { std::cout << "ERROR: Invalid max velocity file" << std::endl; return 0; }
	limits(i) = std::stof(word.c_str());	
      }
    } else if(word == "max_accel"){
      for(int i = 0; i < 3; ++i){
	file >> word;
	if(!isFloat(word) || file == NULL) 
	  { std::cout << "ERROR: Invalid max acceleration file" << std::endl; return 0; }
	limits(i+3) = std::stof(word.c_str());	
      }
    } else if(word == "max_jerk"){
      for(int i = 0; i < 3; ++i){
	file >> word;
	if(!isFloat(word) || file == NULL) 
	  { std::cout << "ERROR: Invalid max jerk file" << std::endl; return 0; }
	limits(i+6) = std::stof(word.c_str());	
      }
    } else if(word == "goal_position"){
      for(int i = 0; i < 3; ++i){
	file >> word;
	if(!isFloat(word) || file == NULL) 
	  { std::cout << "ERROR: Invalid goal position file" << std::endl; return 0; }
	targets(i) = std::stof(word.c_str());	
      }
    } else if(word == "goal_velocity"){
      for(int i = 0; i < 3; ++i){
	file >> word;
	if(!isFloat(word) || file == NULL) 
	  { std::cout << "ERROR: Invalid goal velocity file" << std::endl; return 0; }
	targets(i+3) = std::stof(word.c_str());	
      }
    } else if(word == "goal_rotation"){
      for(int i = 0; i < 9; ++i){
	file >> word;
	if(!isFloat(word) || file == NULL) 
	  { std::cout << "ERROR: Invalid goal rotation file" << std::endl; return 0; }
	R_goal_(i) = std::stof(word.c_str());	
      }
    } else if(word == "hold_pos"){
      file >> word;
      if(!isFloat(word) || file == NULL) 
	{ std::cout << "ERROR: Invalid hold pos flag" << std::endl; return 0; }
      flag_list[0] = std::stof(word.c_str());	
    } else if(word == "straight_traj"){
      file >> word;
      if(!isFloat(word) || file == NULL) 
	{ std::cout << "ERROR: Invalid straight traj flag" << std::endl; return 0; }
      flag_list[1] = std::stof(word.c_str());	
    } else if(word == "use_optitrack"){
      file >> word;
      if(!isFloat(word) || file == NULL) 
	{ std::cout << "ERROR: Invalid use optitrack flag" << std::endl; return 0; }
      flag_list[2] = std::stof(word.c_str());	
    } else if(word == "ball_pos"){
      for(int i = 0; i < 3; ++i){
	file >> word;
	if(!isFloat(word) || file == NULL) 
	  { std::cout << "ERROR: Invalid goal rotation file" << std::endl; return 0; }
	ball_info[i] = std::stof(word.c_str());	
      }
    } else if(word == "ball_radius"){
      file >> word;
      if(!isFloat(word) || file == NULL) 
	{ std::cout << "ERROR: Invalid goal rotation file" << std::endl; return 0; }
      ball_info[3] = std::stof(word.c_str());	
    } else { std::cout << "ERROR: Invalid input file" << std::endl; return 0; }
  }
  return 1;
}

void sendToRobot(Eigen::VectorXd q)
{                          
  // Initialize a 0MQ publisher socket
  static zmqpp::context context;
  static zmqpp::socket pub(context, zmqpp::socket_type::publish);

  if (!zmqIntialized)
    {
      // Need to pair this with the endpoint port in ROS code
      pub.bind("tcp://*:3883");
      zmqIntialized = true;
    }           
  // TODO: Is data lock needed here?
  else
    {
      zmqpp::message msg;
      msg << std::to_string(q[0]) + " " +
	std::to_string(q[1]) + " " +
	std::to_string(q[2]) + " " +
	std::to_string(-q[3]) + " " +  // joint 3 is inverted
	std::to_string(q[4]) + " " +
	std::to_string(q[5]) + " " +
	std::to_string(q[6]);

      pub.send(msg);
    }
}
