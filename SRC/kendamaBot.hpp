/* Kendama Bot
 *
 *  File: KendamaBot.hpp
 * 
 *  Created on: May 10, 2015
 *
 *  Copyright (C) 2015
 *
 *  Author: Ryan Houlihan <ryan.houlihan@stanford.edu> */
#ifndef KENDAMABOT_HPP_
#define KENDAMABOT_HPP_

//scl lib
#include <scl/DataTypes.hpp>
#include <scl/Singletons.hpp>
#include <scl/data_structs/SGcModel.hpp>
#include <scl/dynamics/scl/CDynamicsScl.hpp>
#include <scl/dynamics/tao/CDynamicsTao.hpp>
#include <scl/parser/sclparser/CParserScl.hpp>
#include <scl/graphics/chai/CGraphicsChai.hpp>
#include <scl/graphics/chai/ChaiGlutHandlers.hpp>
#include <scl/control/task/CControllerMultiTask.hpp>
#include <scl/control/task/tasks/data_structs/STaskOpPos.hpp>
//scl functions to simplify dynamic typing and data sharing
#include <scl/robot/DbRegisterFunctions.hpp>
#include <scl/util/DatabaseUtils.hpp>
#include <scl/util/HelperFunctions.hpp>

//Freeglut windowing environment
#include <GL/freeglut.h>

//Reflexxes dependences
#include <ReflexxesAPI.h>
#include <RMLPositionFlags.h>
#include <RMLPositionInputParameters.h>
#include <RMLPositionOutputParameters.h>

// Custom Task
#include "CTaskCustom.hpp"
#include "CTaskOrientationCustom.hpp"
#include "STaskCustom.hpp"
#include "STaskOrientationCustom.hpp"

// Nat Net Lib
#include <NatNetLinux/NatNet.h>
#include <NatNetLinux/CommandListener.h>
#include <NatNetLinux/FrameListener.h>

//Eigen 3rd party lib
#include <Eigen/Dense>

bool launchBallTask(RMLPositionInputParameters*, 
		    RMLPositionOutputParameters*, 
		    ReflexxesAPI*, 
		    RMLPositionFlags&, 
		    scl::CControllerMultiTask&, 
		    scl::CDynamicsTao&, 
		    scl::SRobotIO&, 
		    scl_app::STaskCustom*, 
		    scl_app::STaskOrientationCustom*, 
		    chai3d::cGenericObject*, 
		    Eigen::MatrixBase<Eigen::Vector3d>&, 
		    Eigen::MatrixBase<Eigen::Vector3d>&, 
		    Eigen::VectorXd&, 
		    Eigen::Matrix3d&, 
		    const Eigen::Vector3d&, 
		    const double&, 
		    long long int*, 
		    const double&, 
		    const bool&, 
		    const bool&, 
		    const bool&);

bool catchBallTask(RMLPositionInputParameters*, 
		   RMLPositionOutputParameters*, 
		   ReflexxesAPI*, 
		   RMLPositionFlags&, 
		   scl::CControllerMultiTask&, 
		   scl::CDynamicsTao&, 
		   scl::SRobotIO&, 
		   scl_app::STaskCustom*, 
		   scl_app::STaskOrientationCustom*, 
		   chai3d::cGenericObject*, 
		   Eigen::MatrixBase<Eigen::Vector3d>&, 
		   Eigen::MatrixBase<Eigen::Vector3d>&, 
		   const Eigen::VectorXd&,
		   Eigen::VectorXd&, 
		   Eigen::Matrix3d&, 
		   const Eigen::Vector3d&, 
		   const double&, 
		   long long int*, 
		   const double&, 
		   const bool&, 
		   const bool&, 
		   const bool&);

int _kbhit();
void update_kendama_vel(std::vector<double> *kendama_velocity, 
			std::vector<Point3f> *prev_kendama_pos, 
			const Point3f& current_pos,
			const double& dt);

bool computeKendamaBallTrajectory(Eigen::MatrixBase<Eigen::Vector3d>&,
				  Eigen::MatrixBase<Eigen::Vector3d>&,
				  const Eigen::Vector3d&,
				  const Eigen::Vector3d&,
				  const Eigen::Vector3d&,
				  const Eigen::Vector3d&,
				  const Eigen::Vector3d&);

bool findStraightTrajectory(Eigen::MatrixBase<Eigen::Vector3d>&,
			    const Eigen::Vector3d&,
			    const Eigen::Vector3d&,
			    const double&);

bool updateStraightTrajectory(Eigen::MatrixBase<Eigen::Vector3d>&, 
			      Eigen::MatrixBase<Eigen::Vector3d>&,
			      Eigen::MatrixBase<Eigen::Vector3d>&,
			      const long long int&,
			      const long long int&);

bool findReflexxesTrajectory(const scl_app::STaskCustom&,
			     const Eigen::VectorXd&,
			     RMLPositionInputParameters*);

int updateReflexxesTrajectory(RMLPositionInputParameters*,
			       RMLPositionOutputParameters*,
			       ReflexxesAPI*, 
			       RMLPositionFlags&);
bool areSame(double, double);
bool slowDownSim(double, double);
bool setLimits(RMLPositionInputParameters*, Eigen::VectorXd, int);
bool setPosVelTargets(RMLPositionInputParameters*, Eigen::VectorXd, int);
bool readInputFile(std::string, scl::SRobotIO&, Eigen::MatrixBase<Eigen::VectorXd>&, 
		   Eigen::MatrixBase<Eigen::VectorXd>&, Eigen::MatrixBase<Eigen::VectorXd>&, 
		   Eigen::MatrixBase<Eigen::VectorXd>&, Eigen::MatrixBase<Eigen::Matrix3d>&, 
		   double&, bool*);

void sendToRobot(Eigen::VectorXd);

#endif
